import java.util.Scanner;

public class RegexParsers {

    public static boolean checkColor(String arg) {
        String regexcolor = "Red|Green|Blue|Black|White";
        return arg.matches(regexcolor) || arg.matches(regexcolor.toUpperCase()) || arg.matches(regexcolor.toLowerCase());
    }
    public static boolean checkDate(String arg){
        String regexDate = "[0-3]";
                return arg.matches(regexDate);
    }


    public static void main(String[] args) {
        System.out.println("Wprowadź kolor:");
        Scanner cin = new Scanner(System.in);
        String s = cin.nextLine();

        do {

            if (checkColor(s)) {
                System.out.println("Poprawnie. Dziękuję.");
            } else {
                System.out.println("Niepoprawnie. Spróbuj jeszcze raz!");
            }

        } while (s.equals(""));





    }
}