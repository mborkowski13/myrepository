import java.util.Scanner;

public class ParzystaCzyNie {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println("Wprowadź liczbę do sprawdzenia: ");
        int liczba = in.nextInt();

        if (liczba % 2 == 0) {
            System.out.println("Liczba jest parzysta.");
        } else {
            System.out.println("Liczba jest nieparzysta. ");
        }
    }
}


