public class Person {
    private String firstName;
    private String surname;
    private String sex;
    private int pesel;



    public String getFirstName() {
        return firstName;
    }

    public String getSurname() {
        return surname;
    }
    public String getSex(){
        return sex;
    }
    public int getPesel(){
        return pesel;
    }
    public void setFirstName(String arg){
        firstName =arg;
    }
    public void setSurname(String arg){
        surname=arg;
    }
    public void setSex(String arg){
        sex=arg;
    }
    public void setPesel(int arg){
        pesel=arg;
    }
    public String toString(Person arg){
        return "Imię: "+firstName+ "Nazwisko: "+surname+"płeć: "+sex+ "Pesel: "+ pesel;
    }
}
