import javax.swing.*;

import java.util.Scanner;

import static javax.swing.JOptionPane.*;

public class Calculator {

    public static void main(String[] args) {

        Scanner wejscie = new Scanner(System.in);


        String napis = showInputDialog("Wpisz pierwsza liczbę do obliczeń(liczbę rzeczywista :");
        double liczba1 = Double.parseDouble(napis);
        String napis2 = showInputDialog("Wpisz druga liczbę do obliczeń(liczbę rzeczywista) :");
        double liczba2 = Double.parseDouble(napis2);


        int wybor;

        do {

            String napis3 = showInputDialog("Jakie działanie chcesz wykonać? (1 - dodawanie; 2 - odejmowanie; 3 - możenie; 4 - dzielienie ");
            int dzialanie = Integer.parseInt(napis3);

            switch (dzialanie) {
                case 1:
                    JOptionPane.showMessageDialog(null, "Dodawanie: " + liczba1 + "+" + liczba2 + "=" + (liczba1 + liczba2));
                    break;
                case 2:
                    JOptionPane.showMessageDialog(null, "Odejmowanie: " + liczba1 + "-" + liczba2 + "=" + (liczba1 - liczba2));
                    break;
                case 3:
                    JOptionPane.showMessageDialog(null, "Dzielenie: " + liczba1 + "/" + liczba2 + "=" + (liczba1 / liczba2));
                    break;
                case 4:
                    JOptionPane.showMessageDialog(null, "Mnożenie: " + liczba1 + "*" + liczba2 + "=" + (liczba1 * liczba2));
                default:
                    JOptionPane.showMessageDialog(null, "Nie wybrałeś żadnego działania! ");
            }

            System.out.println("Czy chcesz wykonać działania matematyczne? 1 - TAK; 2- NIE");
            wybor = wejscie.nextInt();
        } while (wybor == 1);
    }
}