public class Obliczenia3{

    private int min = 0;
    private int max = 10;

    public void setMax(int max) {
        this.max = max;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public int getMin() {
        return min;
    }

    private double ogranicz(double a) {
        while (a > max) {
            a = a - (max-min+1);
        }
        while (a < min) {
            a = a + (max-min+1);
        }

        return a;
    }

    public double suma(double a, double b) {
        return ogranicz(a + b);
    }

    public double srednia(double a, double b) {
        return ogranicz(suma(a, b) / 2.0);
    }

    public double minimum(double a, double b) {
        if (a <= b) {
            return ogranicz(a);
        }
        else {
            return ogranicz(b);
        }
    }

    public double maximum(double a, double b) {
        if (a >= b) {
            return ogranicz(a);
        }
        else {
            return ogranicz(b);
        }
    }


    public static void main(String[] args) {
        Obliczenia3 o1, o2;
        o1 = new Obliczenia3();
        o2 = new Obliczenia3();

        o2.setMin(-15);
        o2.setMax(15);

        System.out.println(o1.suma(12, 30));
        System.out.println(o1.suma(-8, -5));
        System.out.println(o1.suma(8, 0));
        System.out.println(o1.suma(-7, 27));

        System.out.println();

        System.out.println(o2.suma(12, 30));
        System.out.println(o2.suma(-8, -5));
        System.out.println(o2.suma(8, 0));
        System.out.println(o2.suma(-7, 27));
    }
}


