import java.time.LocalDate;
import java.util.Scanner;

public class Zegar {


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int dzien, miesiac, rok;

        System.out.println("Podaj swoja datę urodzenia w formacie DD/MM/YYYY: ");
        System.out.println("Dzień: ");
        dzien = in.nextInt();
        System.out.println("Miesiac: ");
        miesiac = in.nextInt();
        System.out.println("Rok: ");
        rok = in.nextInt();

        LocalDate zegar = LocalDate.of(rok, miesiac, dzien);
        LocalDate difference = LocalDate.now();
        difference = difference.minusYears(zegar.getYear());
        difference = difference.minusMonths(zegar.getMonthValue());
        difference = difference.minusDays(zegar.getDayOfMonth());

        System.out.println("Żyjesz już: ");
        System.out.println("  -  " + (difference.getYear() + " lat"));
        System.out.println("  -  " + (difference.getMonthValue() + " miesięcy"));
        System.out.println("  -  " + (difference.getDayOfMonth() + " dni"));

        System.out.println();
        int slubY, slubM, slubD;

        LocalDate now;
        now =LocalDate.now();

        System.out.println("Podaj datę twojego ślubu: ");
        System.out.println("Dzien: ");
        slubD = in.nextInt();
        System.out.println("Miesiac: ");
        slubM = in.nextInt();
        System.out.println("Rok: ");
        slubY = in.nextInt();



        LocalDate slub = LocalDate.of(slubY,slubM,slubD);

        slub = slub.minusYears(now.getYear());
        slub = slub.minusMonths(now.getMonthValue());
        slub = slub.minusDays(now.getDayOfMonth());

        System.out.println("Twój ślub odbędzie się za:");
        System.out.println("  -  " + slub.getYear() + " lat");
        System.out.println("  -  " + slub.getMonthValue() + " miesięcy");
        System.out.println("  -  " + slub.getDayOfMonth() + "dni");


    }

}
