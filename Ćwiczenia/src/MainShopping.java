import com.sun.org.apache.xpath.internal.SourceTree;

import java.time.Duration;
import java.time.Instant;
import java.util.Scanner;

public class MainShopping {

    public static void main(String[] args) {
        Instant date1 = Instant.now();
        Scanner in = new Scanner(System.in);
        int wybor;
        ShoppingBasket main = new ShoppingBasket();

        do {
            System.out.println("Witamy w naszym sklepie. Jaką czynność chcesz wykonać?");
            System.out.println(" 1 - Wyświetl listę dostępnych artykułów. ");
            System.out.println(" 2 - Wybierz produkt i wrzuć go do koszyka.");
            System.out.println(" 3 - Wyświetl zawartość koszyka. ");
            System.out.println(" 4 - Wyświetl cenę do zapłaty. ");
            System.out.println(" 5 - Odejmij ostatni produkt z koszyka. ");
            System.out.println(" 6 - Wyjdź ze sklepu i zapłać. ");
            wybor = in.nextInt();

            switch (wybor) {
                case 1:
                    System.out.println("Artykuły w naszym sklepie: ");
                    Product[] products = new Product[8];
                    products[0] = new Jabłko();
                    products[1] = new FarbaBezwonna();
                    products[2] = new KremDoRąk();
                    products[3] = new Mydło();
                    products[4] = new Narzuta();
                    products[5] = new Spódnica();
                    products[6] = new Wino();
                    products[7] = new Ława();

                    for (int i = 0; i < products.length; i++) {
                        System.out.println((i + 1) + ". = " + products[i]);
                    }

                    break;
                case 2:
                    int wybor2;
                    System.out.println("Jaki produkt chcesz dodać? - Wybierz z listy poniżej: ");
                    System.out.println("");
                    Product[] products1 = new Product[8];
                    products1[0] = new Jabłko();
                    products1[1] = new FarbaBezwonna();
                    products1[2] = new KremDoRąk();
                    products1[3] = new Mydło();
                    products1[4] = new Narzuta();
                    products1[5] = new Spódnica();
                    products1[6] = new Wino();
                    products1[7] = new Ława();

                    for (int i = 0; i < products1.length; i++) {
                        System.out.println((i + 1) + ". = " + products1[i]);
                    }
                    wybor2 = in.nextInt();

                    switch (wybor2) {
                        case 1:
                            System.out.println("Wrzucasz do koszyka jabłko...");
                            main.addProduct(new Jabłko());
                            break;
                        case 2:
                            System.out.println("Wrzucasz do koszyka farbę bezwonną...");
                            main.addProduct(new FarbaBezwonna());
                            break;
                        case 3:
                            System.out.println("Wrzucasz do koszyka krem do rąk...");
                            main.addProduct(new KremDoRąk());
                            break;
                        case 4:
                            System.out.println("Wrzucasz do koszyka mydło...");
                            main.addProduct(new Mydło());
                            break;
                        case 5:
                            System.out.println("Wrzucasz do koszyka narzutę...");
                            main.addProduct(new Narzuta());
                            break;

                        case 6:
                            System.out.println("Wrzucasz do koszyka spódnicę...");
                            main.addProduct(new Spódnica());
                            break;

                        case 7:
                            System.out.println("Wrzucasz do koszyka wino...");
                            main.addProduct(new Wino());
                            break;
                        case 8:
                            System.out.println("Wrzucasz do koszyka ławę ...");
                            main.addProduct(new Ława());
                        default:
                            System.out.println("Proszę wybrać czynność z poniższego menu: ");
                    }
                    break;
                case 3:
                    main.toString();
                    break;
                case 4:
                    System.out.println(main.getMainPrice());
                    break;
                case 5:
                    main.deductProduct();
                    break;
                case 6:
                    System.out.println("Cena za zakupy to: "+main.getMainPrice());
                    System.out.println("Dziękujemy za zakupy i zapraszamy ponownie!");
                    break;
                default:
                    System.out.println("Proszę wybrać czynność z poniższego menu: ");
            }

        } while (wybor !=6);
        Instant date2= Instant.now();
        Duration d = Duration.between(date1, date2);
        System.out.println("Czas twoich zakupów to: "+ d.getSeconds()+ " sekund.");
    }


}
