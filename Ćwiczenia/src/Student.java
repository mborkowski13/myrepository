import java.util.Scanner;


public class Student {
    Scanner in = new Scanner(System.in);

    private String firstName;
    private String lastName;
    private String sex;
    private int[] oceny = new int[5];

    public String getFirstName(){return firstName;}
    public String getLastName(){return lastName;}
    public String getSex(){return sex;}
    public int[] getOceny() { return oceny;}

    public void setFirstName(String arg){firstName = arg;}
    public void setLastName(String arg){lastName = arg;}
    public void setSex(String arg){sex =arg;}

    public void setOceny(int ocena1, int ocena2, int ocena3, int ocena4, int ocena5) {
        oceny [0] = ocena1;
        oceny [1] = ocena2;
        oceny [2] = ocena3;
        oceny [3] = ocena4;
        oceny [4] = ocena5;

    this.oceny = oceny;

    }
   public double averageOceny(){
      double average =(oceny[0]+oceny[1]+oceny[2]+oceny[3]+oceny[4])/5;
      return average;

   }

   public boolean ocenyPass(){
       double average =(oceny[0]+oceny[1]+oceny[2]+oceny[3]+oceny[4])/5;
      if (average>= 3)
       return true;
      else return false;
   }

    public String toString(){
        String result = "Imię : "+firstName+" Nazwisko :"+lastName+" płeć :"+sex;
        return result;
    }
    public Student(){
        firstName = "nie wprowadzono";
        lastName = "nie wprowadzono";
        sex = "nie podano";
}
    public Student(String firstName, String lastName, String sex){
    this.firstName = firstName;
    this.lastName = lastName;
    this.sex = sex;

    }

    public Student(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public static void main(String[] args) {

        Student s1, s2, s3, s4;
       s1= new Student("Michał", "Borkowski", "Mężczyzna");
       s2= new Student();
       s3= new Student("Anna", "Sikorska");
       s4= new Student();

       s1.setOceny(3, 4, 5, 5,3);
        System.out.println(s1.oceny[1]);

        System.out.println(s1.ocenyPass());

    }

}
