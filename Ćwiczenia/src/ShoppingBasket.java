public class ShoppingBasket {

    int numberOfProducts = 0;
    double mainPrice = 0.0;
    Product[] produktyWybrane = new Product[0];


    public String toString() {
        for (int i = 0; i < produktyWybrane.length; i++) {
            System.out.println("Produkt numer " + (1+i) + " = " + produktyWybrane[i]);
        }
        System.out.println("Liczba wybranych produktów to: " + numberOfProducts);
        System.out.println("");
        return null;
    }


    public int getNumberOfProducts() {
        return numberOfProducts;
    }

    public double getMainPrice() {
        return mainPrice;
    }

    public Product[] getProduktyWybrane() {
        return produktyWybrane;
    }

    public void addProduct(Product p1) {
        Product[] temp = new Product[produktyWybrane.length + 1];

        for (int i = 0; i < produktyWybrane.length; i++) {
            temp[i] = produktyWybrane[i];
        }
        temp[temp.length-1] = p1;

        produktyWybrane = temp;
        mainPrice = mainPrice + p1.cena;
        numberOfProducts++;
    }

    public void deductProduct() {

    mainPrice = mainPrice - produktyWybrane[produktyWybrane.length-1].cena;
        Product[] temp = new Product[produktyWybrane.length - 1];

        for (int i = 0; i < produktyWybrane.length - 1; i++) {
            temp[i] = produktyWybrane[i];
        }
        produktyWybrane = temp;
        numberOfProducts--;
    }


}

