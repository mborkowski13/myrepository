import java.util.Random;
import java.util.Scanner;

public class Tablice {

    private int tablica[];
    Scanner in = new Scanner(System.in);
    Random rand = new Random();

    public static int sum(int tablica[]) {

        int suma = 0;

        for (int i = 0; i < tablica.length; i++) {
            suma = suma + tablica[i];
        }
        return suma;
    }

    public static int average(int tablica[]) {

        int average = 0;
        int sum = 0;

        for (int i = 0; i < tablica.length; i++) {
            sum = sum + tablica[i];
        }
        average = sum / tablica.length;
        return average;
    }

    public static int minimum(int tablica[]) {
        int min = tablica[0];

        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] < min) {
                min = tablica[i];
            }
        }
        return min;
    }

    public static int maximum(int tablica[]) {

        int max = tablica[0];

        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] > max) {
                max = tablica[i];
            }
        }
        return max;
    }

    public static int[] getTablica() {
        Scanner in = new Scanner(System.in);

        System.out.println("Wpisz liczbę indeksów do tablicy: ");
        int n = in.nextInt();

        int tablica[] = new int[n];

        for (int i = 0; i < tablica.length; i++) {
            System.out.println("Wpisz liczbę indeksową " + tablica[i] + " do tablicy : ");
            tablica[i] = in.nextInt();

        }
        return tablica;

    }

    public void randomTable() {

        int tablica[] = new int[rand.nextInt()];

        for (int i = 0; i < tablica.length; i++) {
            tablica[i] = rand.nextInt();
        }
    }

    public void stringTablica(int tablica[]) {

        for (int i = 0; i < tablica.length; i++) {
            System.out.println("Element tablicy nr" + i + " {" + tablica[i]);
        }
    }


}


