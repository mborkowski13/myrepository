import com.sun.org.apache.xpath.internal.SourceTree;

import java.util.Random;
import java.util.Scanner;

public class Tablica6 {

    public static void main(String[] args) {

        Scanner user = new Scanner(System.in);
        Random rand = new Random();

        double[] tablica = new double[10];

        int pocz, kon;

        System.out.println("Wpisz początkową liczbę przedziału: ");
        pocz = user.nextInt();
        System.out.println("Wpisz końcową liczbę przedziału: ");
        kon = user.nextInt();

        tablica[0] = pocz;
        tablica[9] = kon;

        for (int i = 1; i < 9; i++) {
            tablica[i] = rand.nextDouble();
        }
        for (int i = 0; i < 9; i++) {
            System.out.println("tablica[" + i + "] = " + tablica[i]);
        }
    }
}
