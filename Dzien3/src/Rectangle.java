public class Rectangle {

    private int x1, y1, x2, y2;

    public Rectangle() {
        x1 = 0;
        y1 = 0;
        x2 = 0;
        y2 = 0;
    }

    public Rectangle(int x1, int y1, int x2, int y2) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }

    public int getX1() {
        return x1;
    }

    public int getY1() {
        return y1;
    }

    public int getX2() {
        return x2;
    }

    public int getY2() {
        return y2;
    }

    public int getX3() {
        return x2;
    }

    public int getY3() {
        return y1;
    }

    public int getX4() {
        return x1;
    }

    public int getY4() {
        return y2;
    }

    public void setX1(int x1) {
        this.x1 = x1;
    }

    public void setY1(int y1) {
        this.y1 = y1;
    }

    public void setX2(int x2) {
        this.x2 = x2;
    }

    public void setY2(int y2) {
        this.y2 = y2;
    }

    public void setX3(int x3) {
        x2 = x3;
    }

    public void setY3(int y3) {
        y1 = y3;
    }

    public void setX4(int x4) {
        x1 = x4;
    }

    public void setY4(int y4) {
        y2 = y4;
    }

    public int getWidth() {
        return Math.abs(x1 - x2);
    }

    public int getHeight() {
        return Math.abs(y1 - y2);
    }

    public String toString() {
        return "{" + x1 + ", " + y1 + "} -> {" + x2 + ", " + y2 + "}";
    }

    public boolean equals(Rectangle r) {
        return (getWidth() == r.getWidth() && getHeight() == r.getHeight());
    }

    public int area() {
        return getWidth() * getHeight();
    }

    public int circuit() {
        return 2* (getWidth() + getHeight());
    }

    public boolean isSquare() {
        return (getWidth() == getHeight());
    }
}