import java.util.Scanner;

public class TestPoint {
    private static String menu = "Cześć. CO chcesz zrobić?\n" +
            "1. Wprowadź dane punktu 1\n" +
            "2. Wprowadź dane punktu 2\n" +
            "3. Przesuń punkt 1 o wektor\n" +
            "4. Przesuń punkt 2 o wektor\n" +
            "5. Oblicz odległość między punktami\n" +
            "6. Wyświetl wszystkie dane\n" +
            "0. Zakończ.\n";

    public static void main(String[] args) {
        int choice;
        Scanner cin = new Scanner(System.in);
        Point p1, p2;
        p1 = new Point();
        p2 = new Point();
        int a, b;

        do {
            System.out.println(menu);
            choice = cin.nextInt();
            switch (choice) {
                case 1:
                    System.out.println("Wprowadź X: ");
                    p1.setWspolrzedne1(cin.nextInt());
                    System.out.println("Wprowadź Y: ");
                    p1.setWspolrzedne2(cin.nextInt());
                    break;
                case 2:
                    System.out.println("Wprowadź X: ");
                    p2.setWspolrzedne1(cin.nextInt());
                    System.out.println("Wprowadź Y: ");
                    p2.setWspolrzedne2(cin.nextInt());
                    break;
                case 3:
                    System.out.println("Wprowadź X: ");
                    a = (cin.nextInt());
                    System.out.println("Wprowadź Y: ");
                    b = (cin.nextInt());
                    p1.move(a, b);
                    break;
                case 4:
                    System.out.println("Wprowadź X: ");
                    a = (cin.nextInt());
                    System.out.println("Wprowadź Y: ");
                    b = (cin.nextInt());
                    p2.move(a, b);
                    break;
                case 5:
                    System.out.println("Odległość: " + p1.distance(p2));
                    break;
                case 6:
                    System.out.println("Punkt 1 : " + p1 + ", ćwiartka: " + p1.quarter());
                    System.out.println("Punkt 2 : " + p2 + ", ćwiartka: " + p2.quarter());
                    break;
                case 0:
                    System.out.println("Do widzenia!");
                    break;
            }
        } while (choice != 0);

    }
}
