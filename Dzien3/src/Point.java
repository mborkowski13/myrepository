public class Point extends Figure {

    private double x, y;

    public Point() {
        super(FigureType.POINT);
        x = 0.0;
        y = 0.0;
    }

    public Point(double x, double y) {
        super(FigureType.POINT);
        this.x = x;
        this.y = y;
    }

    public Point(Point point) {
        super(FigureType.POINT);
        this.x = point.x;
        this.y = point.y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public Point[] getEdges() {
        Point[] result = new Point[1];
        result[0] = this;
        return result;
    }
}