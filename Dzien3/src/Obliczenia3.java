public class Obliczenia3 {

        private String firstName;
        private String lastName;
        private int age;

        public static int counter =0;

        public String getFirstName() {return firstName;}
        public String getLastName() {return lastName;}

        public int getAge() {
            return age;
        }
        public void setFirstName(String firstName){
            this.firstName = firstName;
        }
        public void setLastName(String lastName){
            this.lastName = lastName;
        }
        public void setAge(int age){
            this.age = age;
        }
        public String toString(){
            return firstName + " "+ lastName + ", lat: "+age;
        }

        public static void main(String[] args) {
            Student s1, s2, s3;

            s1=new Student();
            s2=new Student();
            s3=new Student();

            s1.setFirstName("MIchał");
            s1.setLastName("B.");
            s1.setAge(20);

            s2.setFirstName("Lukasz");
            s2.setLastName("G.");
            s2.setAge(25);


            s3.setFirstName("Marian");
            s3.setLastName("B.");
            s3.setAge(27);

            System.out.println(s1.toString());
            System.out.println(s2);
            System.out.println(s3);

        }



    }

