public class Rectangle {

    private Point p1, p2;

    public Rectangle() {
        p1 = new Point();
        p2 = new Point();
    }

    public Rectangle(int x1, int y1, int x2, int y2) {
        p1 = new Point(Math.min(x1, x2), Math.min(y1, y2));
        p2 = new Point(Math.max(x1, x2), Math.max(y1, y2));
    }

    public Rectangle(Point a, Point b) {
        p1 = new Point(a);
        p2 = new Point(b);
    }

    public Rectangle(Rectangle r) {
        p1 = new Point(r.p1);
        p2 = new Point(r.p2);
    }

    public String toString() {
        return "" + p1 + " -> " + p2;
    }

    public Point getP1() {
        return p1;
    }

    public Point getP2() {
        return p2;
    }

    public void setP1(int x1, int y1) {
        int x2 = p2.getX();
        int y2 = p2.getY();

        p1.setX(Math.min(x1, x2));
        p1.setY(Math.min(y1, y2));
        p2.setX(Math.max(x1, x2));
        p2.setY(Math.max(y1, y2));
    }

    public void setP2(int x2, int y2) {
        int x1 = p1.getX();
        int y1 = p1.getY();

        p1.setX(Math.min(x1, x2));
        p1.setY(Math.min(y1, y2));
        p2.setX(Math.max(x1, x2));
        p2.setY(Math.max(y1, y2));
    }

    public int getWidth() {
        return p2.getX() - p1.getX();
    }

    public int getHeight() {
        return p2.getY() - p1.getY();
    }

    public int getArea() {
        return getWidth() * getHeight();
    }

    public int getCircuit() {
        return 2 * (getWidth() + getHeight());
    }

    public boolean isSquare() {
        return getWidth() == getHeight();
    }

    public boolean isOverlapped(Rectangle x) {
        if (x.getP1().getX() >= p1.getX() && x.getP1().getX() <= p2.getX()
                && x.getP1().getY() >= p1.getY() && x.getP1().getY() <= p2.getY()) {
            return true;
        }
        if (x.getP2().getX() >= p1.getX() && x.getP2().getX() <= p2.getX()
                && x.getP2().getY() >= p1.getY() && x.getP2().getY() <= p2.getY()) {
            return true;
        }
        if (x.getP1().getX() >= p1.getX() && x.getP1().getX() <= p2.getX()
                && x.getP2().getY() >= p1.getY() && x.getP2().getY() <= p2.getY()) {
            return true;
        }
        if (x.getP1().getX() >= p1.getX() && x.getP1().getX() <= p2.getX()
                && x.getP2().getY() >= p1.getY() && x.getP2().getY() <= p2.getY()) {
            return true;
        }

        return false;
    }

    public boolean isOverlappedByFor(Rectangle x) {
        //METODA SILOWA:
        for (int i = p1.getX(); i <= p2.getX(); i++) {
            for (int j = p1.getY(); j <= p2.getY(); j++) {
                if (x.getP1().getX() == i && x.getP1().getY() == j) {
                    return true;
                }
                if (x.getP2().getX() == i && x.getP2().getY() == j) {
                    return true;
                }
                if (x.getP1().getX() == i && x.getP2().getY() == j) {
                    return true;
                }
                if (x.getP2().getX() == i && x.getP1().getY() == j) {
                    return true;
                }
            }
        }

        return false;
    }


    // CZĘŚĆ TESTUJĄCA:

    public static void main(String[] args) {
        Rectangle r = new Rectangle(7, 5, 2, 9);
        System.out.println(r);

        System.out.println("Lewy dolny róg: " + r.getP1());
        System.out.println("Prawy górny róg: " + r.getP2());

        r.setP1(20, 30);
        System.out.println(r);

        System.out.println("Szerokość: " + r.getWidth());
        System.out.println("Wysokość: " + r.getHeight());
        System.out.println("Pole: " + r.getArea());
        System.out.println("Obwód: " + r.getCircuit());
        System.out.println("Kwadrat: " + r.isSquare());

        Rectangle r2 = new Rectangle(6, 8, 4, 2);
        Rectangle r3 = new Rectangle(16, 18, 4, 2);
        System.out.println(r);
        System.out.println(r2);
        System.out.println("Nachodzą: " + r.isOverlapped(r2));
        System.out.println("Nachodzą: " + r.isOverlappedByFor(r2));
        System.out.println(r);
        System.out.println(r3);
        System.out.println("Nachodzą: " + r.isOverlapped(r3));
        System.out.println("Nachodzą: " + r.isOverlappedByFor(r3));
    }

}