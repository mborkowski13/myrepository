import java.util.Scanner;

public class TShirt {

public enum TShirtSizes {None, S, M, L, XL, XXL};
public enum TshirtColor {White, Black, Blue, Red, Green};

private TShirtSizes size;
private TshirtColor color;
private String producer;
private String model;

public TShirt(){
    size = TShirtSizes.M;
    color = TshirtColor.Black;
    producer = "Made in China";
    model = "universal";
}
public TShirt(TShirtSizes size, TshirtColor color, String producer, String model){
    this.size = size;
    this.color = color;
    this.producer = producer;
    this.model = model;
}

public String toString(){
    return""+size+" "+color+"  " +producer+"model: "+model;
}

//TESTOWANIE
public static void main(String[] args) {
    TShirt t1 = new TShirt();
    TShirt t2 = new TShirt(TShirtSizes.XXL, TshirtColor.Green,"amsdi", "Big");
    System.out.println(t1);
    System.out.println(t2);

    Scanner in = new Scanner(System.in);
    String size, color, producer, model;

    System.out.println("Twoja wymarzona koszulka: ");
    System.out.println("Rozmiar ");
}
}



