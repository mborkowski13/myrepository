
public class Point {

    private int x;
    private int y;

    public Point() {
        x = 0;
        y = 0;
    }

    public Point(int newX, int newY) {
        x = newX;
        y = newY;
    }

    public Point(Point p) {
        x = p.x;
        y = p.y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int newX) {
        x = newX;
    }

    public void setY(int newY) {
        y = newY;
    }

    public void set(int newX, int newY) {
        x = newX;
        y = newY;
    }

    public String toString() {
        String result = "{" + x + ", " + y + "}";
        return result;
    }

    public boolean equals(Point p) {
        boolean result;
        if (x == p.x && y == p.y) {
            result = true;
        }
        else {
            result = false;
        }
        return result;
    }

    public void movePoint(int mx, int my) {
        x = x + mx;
        y = y + my;
    }

    public void clear() {
        x = 0;
        y = 0;
    }

    public double distance(Point p) {
        double result = (p.x - x)*(p.x - x);
        result = result + (p.y - y)*(p.y - y);
        result = Math.sqrt(result);
        return result;
    }

    public int quarter() {
        if (x >= 0 && y >= 0) {
            return 1;
        }
        else if (x < 0 && y >= 0) {
            return 2;
        }
        else if (x < 0 && y < 0) {
            return 3;
        }
        else {
            return 4;
        }
    }
}

