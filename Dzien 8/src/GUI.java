import javax.swing.*;
import java.awt.*;


public class GUI {

    public static void main(String[] args) {
        JFrame window = new JFrame("Hello World!");
        window.setSize(400, 600);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JButton b = new JButton("Clickme, please!");
        window.add(b);
        window.setVisible(true);
    }
}
