import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Calculator extends JFrame implements ActionListener {

    JTextField textArea1, textArea2;
    JButton bAction1, bAction2, bAction3, bAction4;
    JLabel label1, label2, label3;

    public Calculator() {
        super("Hello World");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(500, 600);
        setLocation(50, 50);
        setLayout(new FlowLayout());

        bAction1 = new JButton("+");
        bAction1.addActionListener(this);
        bAction2 = new JButton("-");
        bAction2.addActionListener(this);
        bAction3 = new JButton("*");
        bAction3.addActionListener(this);
        bAction4 = new JButton("/");
        bAction4.addActionListener(this);

        textArea1 = new JTextField();
        textArea2 = new JTextField();


        label1 = new JLabel("Czynnki 1: ");
        label2 = new JLabel("Czynnki 2: ");
        label3 = new JLabel("Wynik: - ");


        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(bAction1)) {
            System.out.println("akcja 1");
        }
        if (e.getSource().equals(bAction2)) {
            System.out.println("akcja 2");
        }
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Actions();
            }
        });
    }
}
