import java.util.InputMismatchException;
import java.util.Scanner;

public class SimpleIntegerBox {

    public int size;
    public int[] boxes;
    public SimpleIntegerBox(int size){
        this.size = size;
        boxes = new int [size];
    }
    public int get (int index){
        return boxes[index];
    }
    public void set (int index, int value) throws NegativeNumberException, OutOfBoxException {
        if (value < 0) {
            throw new NegativeNumberException(value);
        }
        if (index < 0) throw new OutOfBoxException(index);
        if (index> size-1)
            throw new OutOfBoxException(index);
        boxes[index]= value;
    }
    public String toString(){
        StringBuilder result = new StringBuilder();
        for(int value :boxes){
            result.append("[" );
            result.append(value);
            result.append("]");
        }
        return result.toString();
    }

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        SimpleIntegerBox box = new SimpleIntegerBox(5);
        int index, value;
        while (true){
            try {
                System.out.println("Gdzie? ");
                index = cin.nextInt();
                System.out.println("Co? ");
                value = cin.nextInt();

                box.set(index, value);
                System.out.println(box);
            }
            catch (OutOfBoxException e){
                System.out.println("Wyszedłeś poza pudełko o" +e.getPrzekroczonyZakres()+" Spróbuj jeszcze raz.");
            }
            catch (InputMismatchException e){
                System.out.println("Błąd wprowadzanych danych.");
                cin.nextLine(); // BUFOR PRZECZYSCIC ZEBY ON NAM FUNKCJONOWAL@@@@@@@@@@
            }
            catch (NegativeNumberException e){
                System.out.println("Wprowadzona liczba nie moze byc ujemna. Wprowadziłeś: "+e.getNegativeNumber());
            }
        }
    }

}
