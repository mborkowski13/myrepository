import java.util.Scanner;

public class TestTextThread {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String tekst;
        int milisekundy;

        System.out.println("Podaj tekst jaki chcesz wyświetlać: ");
        tekst = in.nextLine();
        System.out.println("Podaj odstęp czasu w milisekundach: ");
        milisekundy = in.nextInt();


        TextThread t1 = new TextThread(tekst, milisekundy);

        Thread thread = new Thread(t1);
        Thread thread1 = new Thread(t1);

        t1.run();
        System.out.println("Poczekamy chwile...");
        System.out.println("Poczekamy chwile...");
        System.out.println("Poczekamy chwile...");
        thread1.start();
    }
}
