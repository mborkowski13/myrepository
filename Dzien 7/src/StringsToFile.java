import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;


/**
 * Klasa przechowujaca maksymalnie 10 napisów;
 * umożliwiająca ich zapis/odczyt do/z pliku tekstowego.
 *
 *
 * @author Java 5 Course by SDA
 * @version 1.0
 */



public class StringsToFile {


    /** tablica przechowywania napisów */
    public static String[] strings = new String[10];

    /** Metoday dodająca nowy napis do tablicyl
     * @param s - napis do dodania
     *
     */

    public static void addString(String s) {
        for (int i = 0; i < strings.length; i++) {
            if (strings[i] == null) {
                strings[i] = s;
                return;
            }
        }
    }

    /** Metoda usuwająca z tablicy wszystkie wystapienia napisu podanego jak parametr.
     * @param s - napis do usunięcia
     */
    public static void removeString(String s) {
        for (int i = 0; i < strings.length; i++) {
            if (strings[i].equals(s)) {
                strings[i] = null;
                return;
            }
        }
    }

    /** metoda zapisujaca tablice do pliku podanego jako parametr. Plik musi istniec.
     *
     * @param filename - nazwa i ścieżka do istniejacego pliku do zapisu
     *
     * @throws FileNotFoundException
     *
     * @see StringsToFile
     */


    public static void saveToFile(String filename) throws FileNotFoundException {
        PrintWriter writer = new PrintWriter(filename);
        for (int i = 0; i < strings.length; i++) {
            if (strings[i] != null) {
                writer.println(strings[i]);
            }
        }
        writer.close();
    }


    /**
     * Metoda wczytująca tablice z pliku podanego jak parametr. Plik musi istniec.
     * @param filename
     * @throws FileNotFoundException
     */
    public static void readFromFile(String filename) throws FileNotFoundException {
        File file = new File(filename);
        Scanner fileIn = new Scanner(file);

        int i = 0;
        while (fileIn.hasNextLine()) {
            strings[i] = fileIn.nextLine();
            i++;

            if (i == strings.length) return;
        }
    }

    /** Metoda wypisujaca zawartosc tablicy na ekran (napis rozdzielone znakiem nowej linii)
     */
    public static void print() {
        for (int i = 0; i < strings.length; i++) {
            if (strings[i] != null) {
                System.out.println(strings[i]);
            }
        }
    }

    /**
     * Prosty moduł testujący.
     * Program wczytuje dane z pliku "strings.txt", pozwala użytkownikowi na dodanie dowolnej ilosci napisow,
     * konczy wprowadzenie gdy uzytkownik
     * @param args
     * @throws FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException {
        Scanner cin = new Scanner(System.in);
        String line;

        readFromFile("D:/strings.txt");
        print();

        System.out.println();
        System.out.println("Teraz mozesz dodac nowe napisy: ");

        do {
            line = cin.nextLine();
            if (! line.equals("")) {
                addString(line);
            }
        } while (! line.equals(""));

        print();
        saveToFile("D:/strings.txt");
    }

}