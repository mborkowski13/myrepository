public interface Zwierze {
    public String jedz();
    public Zwierze rozmnozSie();
    public void rosnij (double masa);

    public default void piszcz(){}

}
