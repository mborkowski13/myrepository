public class OutOfBoxException extends Exception {

    private int przekroczonyZakres;

    public OutOfBoxException(int przekroczonyZakres){


        this.przekroczonyZakres = przekroczonyZakres;

    }

    public int getPrzekroczonyZakres() {
        return przekroczonyZakres;
    }
}
