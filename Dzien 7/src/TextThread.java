public class TextThread implements Runnable {

    private String text;
    private int miliseconds;

    public TextThread( String text, int miliseconds){
        this.miliseconds = miliseconds;
        this.text = text;

    }
    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(miliseconds);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(text);

        }
    }
}
