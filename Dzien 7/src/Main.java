import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        double a, b;

        System.out.println("Podaj liczbę a: ");
        a = in.nextDouble();
        System.out.println("Podaj liczbę b: ");
        b = in.nextDouble();

        Multiplication multi = new Multiplication();
        Exponentiation expo =new Exponentiation();



        System.out.println(" a*b = "+multi.compute(a,b));
        System.out.println(" a^b = "+expo.compute(a,b));

    }
}
