public class Exponentiation implements Computation {

    @Override
    public double compute(double a, double b) {
        return Math.pow(a,b);
    }
}
