public class Circle extends Point {

    double radius;

    public Circle() {
        super();
        setType(FigureType.CIRCLE);
        radius = 0.0;
    }

    public Circle(double x, double y, double radius) {
        super(x, y);
        setType(FigureType.CIRCLE);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public String toString() {
        return super.toString() + " radius: " + radius;
    }

    public boolean equals(Circle circle) {
        boolean result = super.equals(circle);
        return (result && radius == circle.getRadius());
    }

    @Override
    public double getArea() {
        return Math.PI * radius * radius;
    }

    @Override
    public double getCircuit() {
        return 2 * Math.PI * radius;
    }
}