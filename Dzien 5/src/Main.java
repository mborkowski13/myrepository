public class Main {

    public static void main(String[] args) {
        /*Figure fig = new Figure();
        System.out.println(fig);

        Point p = new Point();
        System.out.println(p);

        System.out.println();
        System.out.println(fig.equals(p));
        System.out.println(p.equals(fig));
        System.out.println(p.equals(p));

        Point p2 = new Point(3, 4);
        System.out.println(p.equals(p2));

        Circle c = new Circle(3, 4, 0);
        Circle c2 = new Circle(3, 4, 5);
        System.out.println(c);
        System.out.println(c2);
        System.out.println(c.equals(c2));
        System.out.println(p2.equals(c));

        System.out.println();

        Line line1 = new Line(new Point(2, 9), new Point(2, 4));
        Line line2 = new Line(new Point(2, 9), new Point(3, 4));
        System.out.println(line1);
        System.out.println(line2);
        System.out.println(line1.equals(line2));
        System.out.println(line2.equals(line2));

        System.out.println();
        Polygon polygon = new Polygon();
        System.out.println(polygon);
        polygon.addEdge(new Point(2, 12));
        polygon.addEdge(new Point(-5, 6));
        polygon.addEdge(new Point(4, -1));
        polygon.addEdge(new Point(2, 5));
        polygon.addEdge(new Point(3, 8));
        System.out.println(polygon);
        polygon.removeEdge(4);
        System.out.println(polygon);
        polygon.setEdge(1, new Point(-10, -10));
        System.out.println(polygon);
        */

        Figure[] figures = new Figure[5];
        figures[0] = new Point();
        figures[1] = new Point(2, 5);
        figures[2] = new Circle(2, 5, 5);
        figures[3] = new Line((Point)figures[1], new Point(2, 4));
        figures[4] = new Polygon();
        figures[4].addEdge(new Point());////////

        for (int i = 0; i < figures.length; i++) {
            System.out.println(figures[i]);
        }
    }

}