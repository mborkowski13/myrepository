public class Varargs {

    public static int suma (int... nums){

        int wynik = 0;

        for (int i = 0; i <nums.length ; i++) {
            wynik+=nums[i];

        }
        return wynik;
    }

    public static void main(String[] args) {
        System.out.println(suma());
        System.out.println(suma(1,3,5,8,12));
        System.out.println(suma(2,4));

        System.out.println();
        
    }

}
