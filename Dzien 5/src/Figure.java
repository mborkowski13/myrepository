public abstract class  Figure {

    public enum FigureType {NONE, POINT, LINE, CIRCLE, RECTANGLE, SQUARE, POLYGON};

    private FigureType type;

    public Figure() {
        type = FigureType.NONE;
    }

    public Figure(FigureType type) {
        this.type = type;
    }

    public FigureType getType() {
        return type;
    }

    protected void setType(FigureType type) {       //tylko obiekty klas pochodnych beda mogly zmodyfikowac typ
        this.type = type;
    }

    @Override
    public String toString() {
        Point[] edges = getEdges();
        if (edges == null) {
            return "Figure: " + type;
        }

        String result = "Figure: " + type + " {";
        for (int i = 0; i < edges.length; i++) {
            result = result + "{" + edges[i].getX() + ", " + edges[i].getY() + "}";
            if (i < edges.length-1) { result = result + ", "; }
        }
        result = result + "}";
        return result;

        //alternatywnie można nadpisać toString w każdej z klas pochodnych
    }

    public boolean equals(Figure f) {
        if (type.equals(f.getType())) {
            Point[] edges1 = getEdges();
            Point[] edges2 = f.getEdges();

            if (edges1.length != edges2.length) { return false; }

            for (int i = 0; i < edges1.length; i++) {
                if (edges1[i].getX() != edges2[i].getX()) { return false; }
                if (edges1[i].getY() != edges2[i].getY()) { return false; }
            }
            return true;
        }
        else {
            return false;
        }

        //alternatywnie można nadpisać equals w każdej z klas pochodnych
    }

    public double getArea() {
        return 0.0;
    }

    public double getCircuit() {
        return 0.0;
    }

    public Point[] getEdges() {
        return null;
    }

    public void addEdge(Point p) {};
}